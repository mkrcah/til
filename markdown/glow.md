# How to render Markdown files in a terminal?

It turns out there is a pretty nice CLI tool to display **rendered** Markdown files in the terminal, called [Glow](https://github.com/charmbracelet/glow).


![](https://camo.githubusercontent.com/59d79838ba1aaa8d2a106a44c00aa37e452528a3/68747470733a2f2f73747566662e636861726d2e73682f676c6f77322d75692d6769746875622e676966)

Intro video is [here](https://www.youtube.com/watch?v=EDNpK3iH7A0)
