# How to improve non-fiction writing?

Here are books that have been helping me in writing:

- **Sönke Ahrens, *How To Take Smart Notes*** ([Goodreads](https://www.goodreads.com/book/show/34507927-how-to-take-smart-notes)), recommended by @Sirupsen. A game-changer. The book has shattered my reality & introduced a new view into writing notes.
- **Steven Pinker, *The Sense of Style*** ([Goodreads](https://www.goodreads.com/book/show/20821371-the-sense-of-style)), recommended by @GergelyOrosz. Another world-shattering book. This time about the style of writing.
- **William Zinsser, *On Writing Well*** ([Goodreads](https://www.goodreads.com/book/show/53343.On_Writing_Well)), recommended by @GergelyOrosz. An insightful complement to Pinker's book.
- **Barbara Minto, *The Pyramid Principle***([Goodreads](https://www.goodreads.com/book/show/33206.The_Minto_Pyramid_Principle)), recommended by the management company I shortly worked. The book advocates for a different, top-down approach. Useful for design docs & functional-specs.

> If you write to improve all of these activities, you have a strong tailwind going for you. If you take your notes in a smart way, it will propel you forward. -- Sönke Ahrens

Here are links that caught my eye:
- [Why Leaders Need to Learn the Skill of Writing](https://fromthegreennotebook.com/2020/10/03/why-leaders-need-to-learn-the-skill-of-writing/) - writing from an army perspective and [this](https://news.ycombinator.com/item?id=24968238) HN comment.
- [How to Write Technical Posts](https://reasonablypolymorphic.com/blog/writing-technical-posts/) and [this](https://news.ycombinator.com/item?id=23840787) & [this](https://www.youtube.com/watch?v=aFwVf5a3pZM) HN comment.
- [A Method for Writing Cohesively](https://jessimekirk.com/blog/writing_cohesively/)
- [The Turkey City Lexicon][tcl] & [Rules for Writers](https://locusmag.com/2020/05/cory-doctorow-rules-for-writers/)
- [Software For Writers: Tools To Improve Your Writing](https://rs.io/software-writers-tools-improve-writing/)
- [Gwern - Writing Checklist](https://www.gwern.net/About#writing-checklist)
- [HN thread](https://news.ycombinator.com/item?id=7772557) about using a dictionary

[tcl]: https://www.sfwa.org/2009/06/18/turkey-city-lexicon-a-primer-for-sf-workshops/
