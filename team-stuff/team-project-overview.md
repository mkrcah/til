# How to capture who works on what?

When discussing & organizing the dev team around projects,  the following table works pretty well:

![](./team-project-overview.png)


Here's how it works:
- Gather  **everything you work on** (projects, maintenance, recurring work) & put these into rows.
- In columns, put dev subteams, for example called squads. (You can also put individual devs).
- Values are true/false, indicating if the squad is responsible for the thing in the row. 

It turns out such a table works pretty well:
- The list of things is insightful by itself: it reveals all the context switching that is required.  
- It is clear who is responsible for an incoming support ticket for a given application.
- New hires get an overview of the team & projects.
- It's easy to keep the table up-to-date: a quick realignment when there is a new project or when we finish stuff.  

   

