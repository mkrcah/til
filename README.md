# Today I Learned

Short write-ups on things I explore day-to-day. Thanks to [Josh](https://github.com/jbranchaud/til) for inspiration.

## Technical stuff

### aws

- [How to automatically resolve code reviews comments related to AWS Cloudformation templates syntax?](./aws/validate-cloudformation-templates.md)

### http

- [How to check two-way authentication SSL/TLS handshake?](./http/check-two-way-auth.md)

### markdown

- [How to render Markdown files in a terminal?](./markdown/glow.md)

## Non-technical stuff
 
### dev team & communication

- [How to capture who works on what?](./team-stuff/team-project-overview.md)
- [How to answer a question from a fellow developer?](./team-stuff/answer-question.md)
- [How to improve non-technical writing?](./team-stuff/writing.md)

### consulting
- [How to level up consulting?](./consulting/levelling-up.md)




