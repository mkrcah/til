# How to check two-way authentication SSL/TLS handshake?



I work on a REST API client that needs to connect to an SSL/TSL server using two-way authentication. That is, the client also needs to authenticate itself with the client SSL certificate. You can quickly check if the SSL handshake can be established with this handy `openssl`  diagnostics command:

```bash
$ openssl s_client -connect example.com:403 -cert client_cert.pem
```

where `example.com:403` is the server, and `client_cert.pem` is the client certificate.

In case of SSL handshake failure, the command will show an error, for example:

```
4306587116:error:1401E0E5:SSL routines:CONNECT_CR_FINISHED:ssl handshake failure:/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-47.11.1/libressl-2.8/ssl/ssl_pkt.c:585:
```

In case the handshake succeeds & the connection is established, the command will be waiting for input, which will be sent to the server.

Some more posts I have found useful about this topic:

-   [OpenSSL s_client docs](https://www.openssl.org/docs/man1.0.2/man1/openssl-s_client.html), especially the "NOTES" section at the bottom
-   [How To Use OpenSSL s_client To Check and Verify SSL/TLS Of HTTPS Webserver](https://www.poftut.com/use-openssl-s_client-check-verify-ssltls-https-webserver/)
-   [OpenSSL Command-Line HOWTO](https://www.madboa.com/geek/openssl/#command-line-clients-and-servers)

![](check-two-way-auth.png)



