# How to level up consulting?

I don't know from experience.  But, researching on how to take my consulting to a new level, I stumbled upon [several insightful discussions on HackerNews](https://hn.algolia.com/?dateRange=all&page=0&prefix=true&query=clients&sort=byPopularity&type=story).

I was particularly inspired by two of Greg Kogan's articles:

- [How to Find Consulting Clients](https://www.gkogan.co/blog/how-i-learned-to-get-consulting-leads/)
- [Bits of Consulting Advice](https://www.gkogan.co/blog/consulting-advice/)

> The strategy of blogging to fill the top of the funnel continues to be effective, for me and my clients.

He also notes blogging might not work for everybody. However, I am intrigued by the long-term prospect of blogging,  compared to other strategies, such as cold calls and networking. Blogging fits nicely in [the important/not-due-soon quadrant](https://www.youtube.com/watch?v=oTugjssqOT0&feature=youtu.be&t=1256).  Andrew Chen [raises a similar point](https://andrewchen.co/professional-blogging/):

> Writing is the most scalable professional networking activity – stay home, don’t go to events/conferences, and just put ideas down.

As a side note, I also like the design of Greg's webpage: simple & right to the point. Similar to [Julia's website](https://jvns.ca/). 

For reference, here are articles/books about consulting I have found in the past that have resonated with me: 

- [Don't Call Yourself A Programmer, And Other Career Advice](https://www.kalzumeus.com/2011/10/28/dont-call-yourself-a-programmer/)
- [Positioning Manual for Indie Consultants](https://philipmorganconsulting.com/the-positioning-manual-for-technical-firms/)
- [How to start freelancing and get clients](https://typicalprogrammer.com/how-to-start-freelancing-and-get-clients)
