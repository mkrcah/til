# How to answer a question from a fellow developer?

Julia Evans wrote this great post on [How to answer questions in a helpful way](https://jvns.ca/blog/answer-questions-well/). I'd say it's best to go ahead and read the whole post.

There was one section though which startled me:

>People often come and ask my team the same questions over and over again. This is obviously not the fault of the people (how should they know that 10 people have asked this already, or what the answer is?). So we’re trying to, instead of answering the questions directly,
>
> 1. Immediately write documentation
> 2. Point the person to the new documentation we just wrote
> 3. Celebrate!
>
> Writing documentation sometimes takes more time than just answering the question, but it’s often worth it! Writing documentation is especially worth it if:
>
> a. It’s a question which is being asked again and again
> b. The answer doesn’t change too much over time (if the answer changes every week or month, the documentation will just get out of date and be frustrating)

I really like this approach. It sits so well with the important/not-due-soon quadrant, that Randy Pausch passionately explains in his [Time Management lecture](https://youtu.be/oTugjssqOT0?t=1256).

There's also  early validation built in. When I sent this article to a fellow developer, he answered:	
> Yeah that makes total sense. I think people are sometimes not motivated to write documention because it is hard to know what is relevant for other people. In this scenario that validation is already there. Plus you have the possibility to validate the clarity of the documentation right that instance as well. (If the other person gets it, it is clear)

And it also aligns with how & why they write [GitLab Handbook](https://about.gitlab.com/handbook/): 

> At GitLab our handbook is extensive and keeping it relevant is an important part of everyone's job. It is a vital part of who we are and how we communicate. We established these processes because we saw these benefits:
>
> - Reading is much faster than listening.
> - Reading is async, you don't have to interrupt someone or wait for them to become available.
> - Recruiting is easier if people can see what we stand for and how we operate.
> - Retention is better if people know what they are getting into before they join.
> - On-boarding is easier if you can find all relevant information spelled out.
> - Teamwork is easier if you can read how other parts of the company work.
> - Discussing changes is easier if you can read what the current process is.
> - Communicating change is easier if you can just point to the diff.
> - Everyone can contribute to it by proposing a change via a merge request.
>
>One common concern newcomers to the handbook express is that the strict documentation makes the company more rigid. In fact, writing down our current process in the handbook has the effect of empowering contributors to propose change. As a result, this handbook is far from rigid. You only need to look at the handbook changelog to see the evidence.


