# How to automatically resolve code review comments related to AWS Cloudformation templates syntax?
 
It turns out there is handy [CLI tool to validate AWS Cloudfromation templates](https://github.com/aws-cloudformation/cfn-python-lint), called `cfn-lint`.

Given a set of YAML/JSON templates, ``cfn-lint`` runs the file against the Cloudformation template specification & checks for best practices.  

The tool catches many opportunities where the template can be simplified. Here's an example from the current project:

```shell
$ cfn-lint cloudformation-stacks/graphql-api.yml 

W1020 Fn::Sub isn't needed because there are no variables at Resources/AdminLambda/Properties/Policies/2/SSMParameterReadPolicy/ParameterName/Fn::Sub
cloudformation-stacks/graphql-api.yml:229:3

W3005 Obsolete DependsOn on resource (GraphQLRole), dependency already enforced by a "Fn:GetAtt" at Resources/AdminDataSource/Properties/ServiceRoleArn/Fn::GetAtt
cloudformation-stacks/graphql-api.yml:244:9

W3005 Obsolete DependsOn on resource (AdminLambda), dependency already enforced by a "Fn:GetAtt" at Resources/AdminDataSource/Properties/LambdaConfig/LambdaFunctionArn/Fn::GetAtt
cloudformation-stacks/graphql-api.yml:245:9

``` 

It might be a useful to run the linter as a git pre-commit hook. There are also editor plugins available. 

 